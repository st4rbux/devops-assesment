Steps
* create a new GCP Project
> st4rbux@cloudshell:~$ gcloud projects create devops-assessment-2020
> Create in progress for [https://cloudresourcemanager.googleapis.com/v1/projects/devops-assessment-2020].
> Waiting for [operations/cp.5498694421571525766] to finish...done.
> Enabling service [cloudapis.googleapis.com] on project [devops-assessment-2020]...
> Operation "operations/acf.0902a0dd-59f9-44f4-b5fd-04610b1494d2" finished successfully.

$ gcloud config set project devops-assessment-2020

* enable billing on the new project; (must be done in web Console?)
* enable container services in the project:
$ gcloud services enable container.googleapis.com

* create the k8s cluster

$ gcloud beta container --project "devops-assessment-2020" clusters create "cluster-1" --zone "us-central1-c" --no-enable-basic-auth --cluster-version "1.16.13-gke.401" --machine-type "e2-medium" --image-type "COS" --disk-type "pd-standard" --disk-size "100" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --num-nodes "3" --enable-stackdriver-kubernetes --enable-ip-alias --network "projects/devops-assessment-2020/global/networks/default" --subnetwork "projects/devops-assessment-2020/regions/us-central1/subnetworks/default" --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0

WARNING: Warning: basic authentication is deprecated, and will be removed in GKE control plane versions 1.19 and newer. For a list of recommended authentication methods, see: https://cloud.google.com/kubernetes-engine/docs/how-to/api-server-authentication
WARNING: Starting with version 1.18, clusters will have shielded GKE nodes by default.
WARNING: The Pod address range limits the maximum size of the cluster. Please refer to https://cloud.google.com/kubernetes-engine/docs/how-to/flexible-pod-cidr to learn how to optimize IP address allocation.
Creating cluster cluster-1 in us-central1-c... Cluster is being configured...⠼

$ gcloud auth configure-docker && docker pull marketplace.gcr.io/google/jenkins2:latest
