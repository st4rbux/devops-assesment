## Pre-requisites
* Jenkins 2.266 with NodeJS plugin
* a NodeJS installation configured (in Global Tool Configuration) with the name 'node'
* a Jenkins user/password with admin permissions, to be used with Ansible to push the job config
* a GitHub account

## To enable Jenkins-GitHub integration

Create a GitHub access token
* log into GitHub
* go to https://github.com/settings/tokens
* click the Generate New Token button
* give the token a description
* Select the following Scopes:
  * repo: all
  * admin:repo_hook: all
  * user: read:user and user:email
* click Generate Token
* copy the token, you will need it in the next step and you will not be able to look it up later

To set the GitHub credential for use with the pipeline
* login to your Jenkins server 
* go to http://[your_jenkins_server]:8080/credentials/store/system/domain/_/
* Add Credentials
  * Kind = Username and Password
  * Scope = Global
  * Username = your github username
  * Password = the access token created above
  * ID = github-cred  // important, this is hardocded in the job definition